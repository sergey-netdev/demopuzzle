﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;

namespace DemoPuzzle
{
	public partial class MainPage : UserControl
	{
		const int MATRIX_SIZE = 3; 
		const int BLOCK_SIZE = 200; // in pixels
		const double BLOCK_OPACITY = 0.8;

		Image sourceImage = new Image();
		Image[,] originMatrix = new Image[MATRIX_SIZE, MATRIX_SIZE];
		Image[,] matrix = new Image[MATRIX_SIZE, MATRIX_SIZE];
		int emptyX = MATRIX_SIZE;
		int emptyY = MATRIX_SIZE;

		public MainPage()
		{
			InitializeComponent();

			Board.Width = MATRIX_SIZE * BLOCK_SIZE;
			Board.Height = Board.Width;
			InitMatrix();
			Shuffle_Click(this, null);
		}

		void InitMatrix()
		{
			StreamResourceInfo sr = Application.GetResourceStream(new Uri("img165.jpg", UriKind.Relative));
			var sourceBitmap = new BitmapImage();
			sourceBitmap.SetSource(sr.Stream);
			sourceImage.Source = sourceBitmap;
			sourceImage.Width = MATRIX_SIZE * BLOCK_SIZE;
			sourceImage.Height = sourceImage.Width;

			for (int x = 0; x < MATRIX_SIZE; x++)
			for (int y = 0; y < MATRIX_SIZE; y++)
			{
				if (x == MATRIX_SIZE - 1 && y == MATRIX_SIZE - 1)
					return; // Skip the last block

				var block = Crop(sourceImage, x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
				block.Opacity = BLOCK_OPACITY;
				block.MouseEnter += new MouseEventHandler(MainPage_MouseEnter);
				block.MouseLeave += new MouseEventHandler(MainPage_MouseLeave);
				block.MouseLeftButtonDown += new MouseButtonEventHandler(block_MouseLeftButtonDown);
				originMatrix[x, y] = block;
			}
		}

		void block_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var point = e.GetPosition(Board);
			if (CanMove(point))
			{
				var x = (int)point.X / BLOCK_SIZE;
				var y = (int)point.Y / BLOCK_SIZE;

				matrix[emptyX, emptyY] = matrix[x, y];
				matrix[x, y] = null;
				emptyX = x;
				emptyY = y;

				Draw();
				CheckFinish();
					
				e.Handled = true;
			}
		}

		void MainPage_MouseLeave(object sender, MouseEventArgs e)
		{
			((Image)sender).Opacity = BLOCK_OPACITY; // Reset opacity to initial 
		}

		void MainPage_MouseEnter(object sender, MouseEventArgs e)
		{
			var point = e.GetPosition(Board);
			if (CanMove(point))
				((Image)sender).Opacity = 1; // Highlight the block (no opacity)
		}

		bool CanMove(Point point)
		{
			var x = (int)point.X / BLOCK_SIZE;
			var y = (int)point.Y / BLOCK_SIZE;

			return IsNextEmpty(x - 1, y)
				|| IsNextEmpty(x + 1, y)
				|| IsNextEmpty(x, y - 1)
				|| IsNextEmpty(x, y + 1);
		}

		void CheckFinish()
		{
			for (int x = 0; x < MATRIX_SIZE; x++)
			for (int y = 0; y < MATRIX_SIZE; y++)
			{
				if (matrix[x, y] != originMatrix[x, y])
					return;
			}

			Board.Children.Clear();
			Board.Children.Add(sourceImage);
		}

		bool IsNextEmpty(int x, int y)
		{
			try
			{
				return matrix[x, y] == null;
			}
			catch (IndexOutOfRangeException)
			{
				return false;
			}
		}

		void Draw()
		{
			Board.Children.Clear();

			for (int x = 0; x < MATRIX_SIZE; x++)
			for (int y = 0; y < MATRIX_SIZE; y++)
			{
				var block = matrix[x, y];
				if (block != null)
				{
					block.SetValue(Canvas.LeftProperty, (double)x * BLOCK_SIZE);
					block.SetValue(Canvas.TopProperty, (double)y * BLOCK_SIZE);
					Board.Children.Add(block);
				}
			}
		}

		public Image Crop(Image image, int x, int y, int width, int height)
		{
			TranslateTransform tt = new TranslateTransform();
			tt.X = -x;
			tt.Y = -y;
			WriteableBitmap bitmap = new WriteableBitmap(width, height);
			bitmap.Render(image, tt);
			bitmap.Invalidate();

			var crop = new Image();
			crop.Source = bitmap;
			crop.Width = width;
			crop.Height = height;
			return crop;
		}

		private void Shuffle_Click(object sender, RoutedEventArgs e)
		{
			matrix = (Image[,])originMatrix.Clone();
			var random = new Random();

			for (int x = 0; x < MATRIX_SIZE; x++)
				for (int y = 0; y < MATRIX_SIZE; y++)
				{
					int x1 = random.Next(MATRIX_SIZE);
					int y1 = random.Next(MATRIX_SIZE);
					// Swapping blocks
					var block = matrix[x, y];
					matrix[x, y] = matrix[x1, y1];
					matrix[x1, y1] = block;
					// Remember empty block position
					if (matrix[x, y] == null)
					{
						emptyX = x;
						emptyY = y;
					}
					else
						if (matrix[x1, y1] == null)
						{
							emptyX = x1;
							emptyY = y1;
						}
				}

			Draw();
		}
	}
}
